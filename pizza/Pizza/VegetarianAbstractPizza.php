<?php

class VegetarianAbstractPizza extends AbstractPizza
{
    public function getForbiddenTypes(): array
    {
        return [MeatInterface::class];
    }
}