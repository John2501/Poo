<?php

interface GarnishInterface
{
    public function getName() : string;
}