<?php

final class PizzaDimensions
{
    const SIZE_SM = "15";
    const SIZE_XL = "30";
    const SIZE_XXL = "60";
}