<?php

class Beef implements GarnishInterface, MeatInterface
{
    public function getName(): string
    {
        return "Beef";
    }
}