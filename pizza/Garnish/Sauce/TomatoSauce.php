<?php

class TomatoSauce implements GarnishInterface, SauceInterface
{
    public function getName(): string
    {
        return "Tomato sauce";
    }

}