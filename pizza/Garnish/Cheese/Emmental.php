<?php

class Emmental implements GarnishInterface, CheeseInterface
{
    public function getName() : string
    {
        return 'Emmental';
    }

}