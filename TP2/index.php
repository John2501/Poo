<?php
/**
 * Created by PhpStorm.
 * User: john2501
 * Date: 30/05/2017
 * Time: 15:57
 */


include "classes/Player.php";
include "classes/Footballer.php";
include "classes/GoalKeeper.php";



$foot1= new Footballer(1,"Robert");
$foot2= new Footballer(2,"Timmy");
$gardien= new GoalKeeper(3,"Gobols");


echo $foot1->run();
echo "<br>";
echo $foot2->drop();
echo "<br>";
echo $gardien->stop();