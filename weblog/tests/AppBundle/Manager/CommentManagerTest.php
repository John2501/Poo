<?php

namespace Tests\Manager;

use AppBundle\Entity\Comment;
use AppBundle\Manager\CommentManager;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class CommentManagerTest extends TestCase{



    public function testSave(){
        $comment = new Comment();

        $em=$this
            ->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->getMock();

        $em->expects($this->once())->method("persist")->with($comment)->willReturn($em);
        $em->expects($this->once())->method("flush")->willReturn($em);


        $doctrine = $this
            ->getMockBuilder(Doctrine::class)
            ->disableOriginalConstructor()
            ->getMock();



        $doctrine
            ->expects($this->once())
            ->method("getManager")
            ->willReturn($em);


        $commentManager= new CommentManager($doctrine);
        $commentManager->save(new Comment());
    }
}