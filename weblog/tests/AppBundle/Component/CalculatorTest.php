<?php


namespace Test\Component;


use Component\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
        public function testAdd(){
            $calculator= new Calculator();
            $this->assertEquals(12,$calculator->add(6,6));
        }
}