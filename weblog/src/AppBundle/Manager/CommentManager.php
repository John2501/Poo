<?php
namespace AppBundle\Manager;
use AppBundle\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;

/**
 * Created by PhpStorm.
 * User: john2501
 * Date: 23/06/2017
 * Time: 15:11
 */
class CommentManager
{

    private $doctrine;

    /**
     * CommentManager constructor.
     * @param $doctrine
     */
    public function __construct(Doctrine $doctrine)
    {
        $this->doctrine = $doctrine;
    }


    public function save(Comment $comment){
        $em= $this->doctrine->getManager();
        $em->persist($comment);
        $em->flush();
    }

}