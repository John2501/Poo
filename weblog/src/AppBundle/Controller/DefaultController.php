<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $posts = $this->getDoctrine()->getRepository("AppBundle:Post")->findAll();

        return $this->render(
            "@App/Default/index.html.twig",
            ["posts" => $posts]
        );
    }

    /**
     * @Route("/about", name="about")
     */
    public function aboutAction()
    {
        return $this->render("@App/Default/about.html.twig");
    }

    /**
     * @Route("/post/{id}", name="post")
     */
    public function postAction(Post $post, Request $request)
    {
        $comment=new Comment();
        $comment->setPost($post);

        $form= $this->createForm(CommentType::class,$comment);
        $form->handleRequest($request);


        if ($form->isSubmitted()&&$form->isValid()){
           /* $em=$this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();*/

           $this->get("app.comment.manager")->save($comment);

        }
        return $this->render(
            "@App/Default/post.html.twig",
            [
                "post"     => $post,
                "form"     => $form->createView()
            ]
        );
    }
}
