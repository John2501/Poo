<?php
/**
 * Created by PhpStorm.
 * User: john2501
 * Date: 01/06/2017
 * Time: 11:24
 */
interface EngineInterface{
    public function turnOn() :bool ;
    public function turnOff() :bool ;
}