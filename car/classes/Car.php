<?php

/**
 * Created by PhpStorm.
 * User: john2501
 * Date: 30/05/2017
 * Time: 11:17
 */
class Car extends Vehicules
{
    const MAX_SPEED=200;


    private $engine;



    public function __construct(EngineInterface $engine , $defaultcolor="black"){

        $this->engine=$engine;

        parent::__construct($defaultcolor);



    }

    /**
     * @return EngineInterface
     */
    public function getEngine(): EngineInterface
    {
        return $this->engine;
    }

    /**
     * @param EngineInterface $engine
     */
    public function setEngine(EngineInterface $engine)
    {
        $this->engine = $engine;
    }





    /**
     * @return string
     */
    public function getColor ()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }


    function getMaxSpeed(){
        return self::MAX_SPEED;
    }




}