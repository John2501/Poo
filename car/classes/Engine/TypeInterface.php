<?php
/**
 * Created by PhpStorm.
 * User: john2501
 * Date: 01/06/2017
 * Time: 11:37
 */

interface TypeInterface{
    public function getType() :string ;
}