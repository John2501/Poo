<?php

/**
 * Created by PhpStorm.
 * User: john2501
 * Date: 30/05/2017
 * Time: 11:45
 */
class Engine implements EngineInterface
{
    private $engineType;


    public function __construct(TypeInterface $type)
    {
        $this->engineType = $type;
    }


    public function turnOn(): bool
    {

    }

    public function turnOff(): bool
    {

    }
}