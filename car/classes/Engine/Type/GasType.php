<?php
/**
 * Created by PhpStorm.
 * User: john2501
 * Date: 01/06/2017
 * Time: 11:38
 */
class GasType implements TypeInterface{
    const TYPE ="GAS";

    public function getType(): string
    {
        return self::TYPE;
    }
}