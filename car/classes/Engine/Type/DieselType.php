<?php
/**
 * Created by PhpStorm.
 * User: john2501
 * Date: 01/06/2017
 * Time: 11:38
 */
class DieselType implements TypeInterface{
    const TYPE ="DIESEL";

    public function getType(): string
    {
        return self::TYPE;
    }
}