<?php

/**
 * Created by PhpStorm.
 * User: john2501
 * Date: 01/06/2017
 * Time: 10:08
 */
class CarCategory
{
        private $name;
        private $description;

    /**
     * CarCategory constructor.
     * @param $name
     * @param $description
     */
    public function __construct($name, $description)
    {
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getName() :string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) :CarCategory
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription() :string
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description) :CarCategory
    {
        $this->description = $description;
        return $this;
    }



}