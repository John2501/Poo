<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $posts = $this
            ->getDoctrine()
            ->getRepository("AppBundle:Post")
            ->findAll();

        // replace this example code with whatever you need
        return $this->render('base.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'posts'=>$posts
        ]);
    }

    /**
     * @Route("/about", name="About")
     */
    public function AboutAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/about.html.twig');
    }


    /**
     * @Route("/post/{id}", name="Post")
     */
    public function PostAction($id)
    {
        $posts = $this
            ->getDoctrine()
            ->getRepository("AppBundle:Post")
            ->find($id);

        // replace this example code with whatever you need
        return $this->render('default/post.html.twig',[
            "post"=>$posts
        ]);
    }

}
