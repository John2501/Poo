<?php
/**
 * Created by PhpStorm.
 * User: john2501
 * Date: 22/06/2017
 * Time: 10:37
 */

namespace ContactBundle\Controller;






use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends controller
{
    /**
     * @Route("/contact",name="contact")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('ContactBundle:Default:contact.html.twig');
    }
}


