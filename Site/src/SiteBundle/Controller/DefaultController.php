<?php

namespace SiteBundle\Controller;

use Doctrine\ORM\Repository\RepositoryFactory;
use SiteBundle\Entity\Category;
use SiteBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends Controller
{
    /**
     * @Route("/",name="site.homepage")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {


        // On récupère l'annonce $id
        $posts = $this
            ->getDoctrine()
            ->getRepository("SiteBundle:Post")
            ->findAll();

        return $this->render(
            "SiteBundle:Default:index.html.twig",
            [
                "posts" => $posts,

            ]
        );




    }


    /**
     * @Route("/page/{id}",name="page")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pageAction($id)
    {

        $posts = $this
            ->getDoctrine()
            ->getRepository("SiteBundle:Post")
            ->find($id);


        return $this->render('SiteBundle:Default:page.html.twig',[
            "post"=>$posts

        ]);
    }

    /**
     * @Route("/redirectContact", name="redirectContact")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function RedirectContactAction(){

        return $this->redirectToRoute('contact');
    }


    /**
     * @Route("/error",name="404")
     *
     */
    public function noRessourceFoundAction(){
       throw $this->createNotFoundException();
    }

    /**
     * @Route("/category",name="listeCatgory")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function findCategoriAction(){
        // On récupère l'annonce $id
        $category = $this
            ->getDoctrine()
            ->getRepository("SiteBundle:Category")
            ->findAll();

        return $this->render(
            "SiteBundle:Default:categories.html.twig",
            [
                "category" => $category,

            ]
        );
    }

    /**
     * @Route("/ListCategory/{id}",name="CategoriDetail")
     * @param Category $category
     *
     */
    public function CategoriAction(Category $category){

        $posts=$this
            ->getDoctrine()
            ->getRepository("SiteBundle:Post")
            ->findBy(["category"=>$category]);

        return $this->render(
            "SiteBundle:Default:categories.html.twig",
            [
                "posts" => $posts,
                "category"=>$category
            ]
        );

    }
}
