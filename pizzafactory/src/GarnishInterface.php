<?php
namespace Pizza;
interface GarnishInterface
{
    public function getName() : string;
}