<?php
namespace Pizza\Pizza;



use Pizza\AbstractPizza;
use Pizza\Garnish\MeatInterface;

class VegetarianAbstractPizza extends AbstractPizza
{
    public function getForbiddenTypes(): array
    {
        return [MeatInterface::class];
    }
}