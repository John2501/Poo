<?php
namespace Pizza;

/*include "Exception/ForbiddenGarnishException.php";
include  "Exception/MissingIngredientException.php";*/

use Pizza\Exception\ForbiddenGarnishException;
use Pizza\Exception\MissingIngredientException;

abstract class AbstractPizza
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $dimension;

    /**
     * @var float
     */
    private $price;

    /**
     * @var DoughInterface
     */
    private $dough;

    /**
     * @var SauceInterface
     */
    private $sauce;

    /**
     * @var GarnishInterface []
     */
    private $garnish = [];

    /**
     * AbstractPizza constructor.
     *
     * @param string $name
     * @param string $dimension
     * @param float  $price
     */
    public function __construct($name, $dimension, $price)
    {
        $this->name      = $name;
        $this->dimension = $dimension;
        $this->price     = $price;
    }

    /**
     * @return array
     */
    abstract public function getForbiddenTypes() : array;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDimension(): string
    {
        return $this->dimension;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return DoughInterface
     */
    public function getDough(): DoughInterface
    {
        return $this->dough;
    }

    /**
     * @return SauceInterface
     */
    public function getSauce(): SauceInterface
    {
        return $this->sauce;
    }

    /**
     * @return GarnishInterface[]
     */
    public function getGarnish(): array
    {
        return $this->garnish;
    }

    /**
     * @param DoughInterface $dough
     *
     * @return AbstractPizza
     */
    public function setDough(DoughInterface $dough) : AbstractPizza
    {
        $this->dough = $dough;

        return $this;
    }

    /**
     * @param SauceInterface $sauce
     *
     * @return AbstractPizza
     */
    public function setSauce(SauceInterface $sauce) : AbstractPizza
    {
        $this->sauce = $sauce;

        return $this;
    }

    /**
     * @param GarnishInterface $garnish
     *
     * @return AbstractPizza
     * @throws Exception
     */
    public function addGarnish(GarnishInterface $garnish) : AbstractPizza
    {
        if(array_intersect(class_implements($garnish), $this->getForbiddenTypes()))
        {
            throw new ForbiddenGarnishException('Meat is forbidden for ' . __CLASS__);
        }

        if(in_array($garnish,$this->garnish)===false)
        {
            throw new ForbiddenGarnishException('Truc en double ' . __CLASS__);
        }

        $this->garnish[] = $garnish;

        return $this;
    }

    /**
     * @return AbstractPizza
     * @throws MissingIngredientException
     */
    public function getPizza() : AbstractPizza
    {
        if($this->dough == null){
            throw new MissingIngredientException("Dough is missing ! ");
        }

        if($this->sauce == null){
            throw new MissingIngredientException("Sauce is missing ! ");
        }

        if(count($this->garnish) == 0)
        {
            throw new MissingIngredientException("There is no garnish on my pizza !! *!!#@!;?  ");
        }

        return $this;
    }
}