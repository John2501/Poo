<?php
namespace Pizza\Garnish\Meat;
use Pizza\GarnishInterface;
use Pizza\Garnish\MeatInterface;

class Beef implements GarnishInterface, MeatInterface
{
    public function getName(): string
    {
        return "Beef";
    }
}