<?php
namespace Pizza\Garnish\Sauce;

use Pizza\GarnishInterface;
use Pizza\Garnish\SauceInterface;
class TomatoSauce implements GarnishInterface, SauceInterface
{
    public function getName(): string
    {
        return "Tomato sauce";
    }

}