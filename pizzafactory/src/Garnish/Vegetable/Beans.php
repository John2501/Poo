<?php

class Beans implements GarnishInterface, VegetableInterface
{
    public function getName(): string
    {
        return "Beans";
    }
}