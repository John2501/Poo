<?php
namespace Pizza\Garnish\Cheese;
use Pizza\GarnishInterface;
use Pizza\Garnish\CheeseInterface;
class Emmental implements GarnishInterface, CheeseInterface
{
    public function getName() : string
    {
        return 'Emmental';
    }

}