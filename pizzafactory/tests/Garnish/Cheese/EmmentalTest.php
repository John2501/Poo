<?php
namespace Pizza\Tests\Garnish\Cheese;

use PHPUnit\Framework\TestCase;
use Pizza\Garnish\Cheese\Emmental;
use Pizza\GarnishInterface;
use Pizza\Garnish\CheeseInterface;

class EmmentalTest extends TestCase
{
public function testEmentalImplementCheeseInterface(){
    $this->assertInstanceOf(CheeseInterface::class,new Emmental());
}




}