<?php
namespace Pizza\Tests\Dough;

use PHPUnit\Framework\TestCase;
use Pizza\Dough\BaseDough;
use Pizza\DoughInterface;

class BaseDoughTest extends TestCase
{
    public function testDoughtShouldImplementDoughInterface(){
        $this->assertInstanceOf(DoughInterface::class,new BaseDough());
    }
}