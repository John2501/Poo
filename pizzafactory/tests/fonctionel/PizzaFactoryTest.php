<?php

namespace Pizza\Tests\Functional;

use PHPUnit\Framework\TestCase;
use Pizza\AbstractPizza;
use Pizza\Dough\BaseDough;
use Pizza\Garnish\Cheese\Emmental;
use Pizza\Garnish\Sauce\TomatoSauce;
use Pizza\PizzaDimensions;
use Pizza\Tests\Pizza\VegetarianAbstractPizzaTest;

class PizzaFactoryTest extends TestCase{
    public function testDeliverValidation(){
        $vegetarianPizza = new VegetarianAbstractPizzaTest("Veggi", PizzaDimensions::SIZE_XL, 9.90);
        $this-> assertInstanceOf(AbstractPizza::class,$vegetarianPizza);
        $vegetarianPizza
            ->setDough(new BaseDough())

            ->addGarnish(new Emmental())
            ->addGarnish(new \Beans());

        $this->assertInstanceOf(AbstractPizza::class,$vegetarianPizza->getPizza());
    }


}

