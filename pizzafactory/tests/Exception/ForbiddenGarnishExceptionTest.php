<?php
namespace Pizza\Tests\Exception;

use PHPUnit\Framework\TestCase;
use Pizza\Exception\ForbiddenGarnishException;


class ForbiddenGarnishExceptionTest extends TestCase
{
    public function testItShouldExtendException(){
        $this->assertInstanceOf(\Exception::class,new ForbiddenGarnishException());
    }
}