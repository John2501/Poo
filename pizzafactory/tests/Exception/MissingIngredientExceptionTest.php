<?php
namespace Pizza\Tests\Exception;


use PHPUnit\Framework\TestCase;
use Pizza\Exception\MissingIngredientException;

class MissingIngredientExceptionTest extends TestCase
{
    public function testItShouldExtendException(){
        $this->assertInstanceOf(\Exception::class,new MissingIngredientException());
    }

}