<?php

namespace Pizza\Tests\Pizza;

use PHPUnit\Framework\TestCase;
use Pizza\Garnish\MeatInterface;
use Pizza\Pizza\VegetarianAbstractPizza;
use Pizza\PizzaDimensions;

class VegetarianAbstractPizzaTest extends TestCase
{
    public function testVegetable(){
        $pizza= new VegetarianAbstractPizza("Veggie",PizzaDimensions::SIZE_SM,15);
        $this->assertContains(MeatInterface::class,$pizza->getForbiddenTypes());
    }


}