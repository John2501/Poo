<?php
namespace Pizza\Tests;

use PHPUnit\Framework\TestCase;
use Pizza\PizzaDimensions;

class PizzaDimensionsTest extends TestCase
{
    public function testDimension(){
        $this->assertEquals("15",PizzaDimensions::SIZE_SM);
        $this->assertEquals("30",PizzaDimensions::SIZE_XL);
        $this->assertEquals("60",PizzaDimensions::SIZE_XXL);
    }
}