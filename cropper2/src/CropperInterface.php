<?php

namespace cropper;

interface CropperInterface
{
    public function crop (string $path, int $width, int $height);
    public function show();

}