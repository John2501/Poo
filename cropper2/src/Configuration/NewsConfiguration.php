<?php

namespace cropper\Configuration;

use cropper\ConfigurationInterface;

class NewsConfiguration implements ConfigurationInterface {

    public function getDirectory(): string
    {
        return __DIR__."/../../storage/news/";
    }

    public function getImageQuality(): int
    {
        return 80;
    }

    public function getType(): string
    {
        return "jpg";
    }


}