<?php
namespace cropper;

interface ConfigurationInterface{
    public function getDirectory():string ;
    public function getImageQuality():int;
    public function getType():string ;
}