<?php

namespace cropper\Crooper;

use cropper\ConfigurationInterface;
use cropper\CropperInterface;
use cropper\LoaderInterface;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Point;

class ImagineCrooper implements CropperInterface{

    private $loader;

    private $configuration;

    private $imagine;

    private $image;

    public function __construct(ConfigurationInterface $configuration, LoaderInterface $loader)
    {
        $this->configuration = $configuration;
        $this->loader = $loader;
        $this ->imagine =new Imagine();
    }

    public function crop(string $path, int $width, int $height)
    {
        //Verifier que la ressource existe
        $path=$this->loader->getFile($path);

        //chemin de l'image de sortie

        $outpout= sprintf("%simage%s.jpg",$this->configuration->getDirectory(),time());

        //Decouper l'image
        $this->image=$this->imagine->open($path);
        $this->image->crop(new Point(0,0),new Box($width,$height));
        $this->image->save($outpout,array('jpeg_quality'=> $this->configuration->getImageQuality()));

    }

    public function show()
    {
        if($this->image instanceof ManipulatorInterface){
            $this->image->show($this->configuration->getType());
        }

        throw new \Exception("No file processed");
    }


}