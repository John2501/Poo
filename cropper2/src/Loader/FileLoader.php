<?php

namespace cropper\Loader;

use cropper\Exception\FileNotFoundException;
use cropper\LoaderInterface;

class FileLoader implements LoaderInterface{
    public function getFile(string $path): string
    {
        if(is_file($path)){
            return $path;
        }

        throw new FileNotFoundException("fichier non trouvé");
    }

}