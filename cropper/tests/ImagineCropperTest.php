<?php
namespace cropper\Tests;

use cropper\ConfigurationInterface;
use Cropper\Cropper\ImagineCropper;
use cropper\Loader\FileLoader;
use PHPUnit\Framework\TestCase;

class ImagineCropperTest extends TestCase{

    public function testCrop(){
        $fileLoader=new FileLoader();
        $config= $this
            ->getMockBuilder(ConfigurationInterface::class)
            ->getMock();

            $config
                ->expects($this->once())
                ->method("getDirectory")
                ->willReturn(__DIR__."/../Storage/test");

        $config
            ->expects($this->once())
            ->method("getCropWidth")
            ->willReturn(400);

        $config
            ->expects($this->once())
            ->method("getCropHeight")
            ->willReturn(700);

        $config
            ->expects($this->once())
            ->method("getImageQuality")
            ->willReturn(80);


        $cropper= new ImagineCropper($config,$fileLoader);
        $cropper->crop(__DIR__."/../Storage/Newsimage1496831874.jpg");
    }


}