<?php
ini_set("display_errors", 1);
include "../vendor/autoload.php";

use Cropper\Configuration\NewsConfiguration;
use Cropper\Loader\FileLoader;
use Cropper\Cropper\ImagineCropper;

$config=new NewsConfiguration();
$loader=new FileLoader();
$cropper= new ImagineCropper($loader,$config);
$cropper->crop(1000,500,"Desert.jpg");
$cropper->show();
var_dump($config,$loader,$cropper);