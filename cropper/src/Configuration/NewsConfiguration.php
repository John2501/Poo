<?php


namespace Cropper\Configuration;

use Cropper\ConfigurationInterface;

class NewsConfiguration implements ConfigurationInterface {

    public function getDirectory(): string
    {
        return __DIR__."/../../Storage/News";
    }

    public function getImageQuality(): int
    {
        return 80;
        // TODO: Implement getImageQuality() method.
    }
    public function getTypes():string
    {
        return"jpg";
    }
}