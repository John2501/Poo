<?php
/**
 * Created by PhpStorm.
 * User: Bureautique
 * Date: 07/06/2017
 * Time: 10:11
 */
namespace Cropper\Cropper;

use Cropper\CropperInterface;
use Cropper\ConfigurationInterface;
use Cropper\LoaderInterface;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Point;
use PHPUnit\Runner\Exception;

class ImagineCropper implements CropperInterface
{
    /**
     * @var LoaderInterface
     */
    private $loader;

    /**
     * @var ConfigurationInterface
     */
    private $configuration;

    /**
     * ImagineCropper constructor.
     * @param LoaderInterface $loader
     * @param ConfigurationInterface $configuration
     */
    private $imagine;
    private $image;
    public function __construct(LoaderInterface $loader, ConfigurationInterface $configuration)
    {
        $this->loader = $loader;
        $this->configuration = $configuration;
        $this->imagine=new Imagine();
    }


    /**
     * @param int $width
     * @param int $height
     * @param string $path
     */
    public function crop(int $width, int $height,string $path)
    {
        //verifier que la ressource existe
        $path=$this->loader->getFile($path);
        //chemin de l'image en sortie
        $output=sprintf("%simage%s.jpg",$this->configuration->getDirectory(),time());
        //decouper l'image
        $this->image=$this->imagine->open($path);
        $this->image->crop(new Point( 0,0),new Box($width,$height));
        $this->image->save($output,array("jpeg_quality"=>$this->configuration->getImageQuality()));



    }

    /**
     *
     */
    public function show()
    {
        if($this->image instanceof ManipulatorInterface){
            $this->image->show($this->configuration->getTypes());
        }

        throw new Exception("image null");
    }
}