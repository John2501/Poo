<?php
/**
 * Created by PhpStorm.
 * User: Bureautique
 * Date: 07/06/2017
 * Time: 10:25
 */

namespace Cropper\Loader;

use Cropper\LoaderInterface;

class FileLoader implements  LoaderInterface
{

    public function getFile(string $path): string
    {
        if(is_file($path)){
            return $path;
        }
        throw new FileNotFoundException("Fichier Non trouvé");
        // TODO: Implement getFile() method.
    }
}