<?php
/**
 * Created by PhpStorm.
 * User: Bureautique
 * Date: 07/06/2017
 * Time: 10:07
 */
namespace Cropper;

interface ConfigurationInterface
{
    public function getDirectory() : string ;
    public function getImageQuality() : int ;
    public function getTypes(): string ;


}