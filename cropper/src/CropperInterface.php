<?php
/**
 * Created by PhpStorm.
 * User: Bureautique
 * Date: 07/06/2017
 * Time: 10:08
 */
namespace Cropper;

interface CropperInterface
{
    public function crop(int $width, int $height, string $path);
    public function show();
}