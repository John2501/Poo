<?php
/**
 * Created by PhpStorm.
 * User: Bureautique
 * Date: 07/06/2017
 * Time: 10:11
 */
namespace Cropper;

interface LoaderInterface
{
    public function getFile(string $path): string;
}